use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde_json::json;
use rand::Rng;

async fn tarot_reading(option: web::Path<String>) -> impl Responder {
    let mut rng = rand::thread_rng();
    let lucky_number = rng.gen_range(0..100); // generate a random number from 0 to 99
    let lucky_color = match option.as_str() {
        "a" => "Red",
        "b" => "Blue",
        "c" => "Green",
        "d" => "Yellow",
        _ => "Unknown",
    };
    HttpResponse::Ok().json(json!({ "Today your lucky number is ": lucky_number, "Today your lucky color is ": lucky_color }))
}
// this is a test for CICD pipeline

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/tarot/{option}", web::get().to(tarot_reading))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
