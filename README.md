# Continuous Delivery of Rust Microservice

This project was modified from week 4 mini project: Containerize a Rust Actix Web Service

## Part I. Rust Microservice Functionality

### 1. Create a New Rust Project
Open a terminal and type `cargo new indiproj2 --bin` to create a new binary project.

### 2. Add Dependencies
We need add all necessery dependencies under `Cargo.toml` file.
```
[dependencies]
actix-web = "4.5.1"
rand = "0.8.5"
serde_json = "1.0.114"
```

### 3. Create a Simple Web Server
Edit `src/main.rs` to set up a basic web server. This web is called TAROT and can generate a lucky color and a lucky number randomly based on the user's input from the url.
```
async fn tarot_reading(option: web::Path<String>) -> impl Responder {
    let mut rng = rand::thread_rng();
    let lucky_number = rng.gen_range(0..100); // generate a random number from 0 to 99
    let lucky_color = match option.as_str() {
        "a" => "Red",
        "b" => "Blue",
        "c" => "Green",
        "d" => "Yellow",
        _ => "Unknown",
    };
    HttpResponse::Ok().json(json!({ "Today your lucky number is ": lucky_number, "Today your lucky color is ": lucky_color }))
}
```

### 4. Local Test
Use the following code to build a port on the local environment and type `cargo run` to start the test.
```
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/tarot/{option}", web::get().to(tarot_reading))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```
Terminal output when start the local test:
```
   Compiling indiproj2 v0.1.0 (/Users/wanghaotian/indiproj2)
    Finished dev [unoptimized + debuginfo] target(s) in 4.25s
     Running `target/debug/indiproj2`
```
Open a browser and try different URL: http://localhost:8080/tarot/{letter}
![screenshot a](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/a.png?ref_type=heads&inline=false)
![b](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/b.png?ref_type=heads&inline=false)
![c](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/c.png?ref_type=heads&inline=false)
![d](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/d.png?ref_type=heads&inline=false)
![error](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/err.png?ref_type=heads&inline=false)

## Part II. Docker Configuration

### 5. Create a Dockerfile
Create a Dockerfile under the directory of the project.
```
# Stage 1: Build
FROM rust:1.75 as builder
WORKDIR /usr/src/myapp

# Copy source code
COPY ./ ./

# Install necessary tools and add musl target
RUN apt-get update && apt-get install -y musl-tools

# RUN cargo test

RUN rustup target add x86_64-unknown-linux-musl

# Build statically linked binaries for Alpine
RUN cargo build --release --target x86_64-unknown-linux-musl

# Stage 2: Run
FROM alpine:latest
WORKDIR /root/

#Install runtime dependencies
RUN apk --no-cache add ca-certificates

# Copy the built app from the builder
COPY --from=builder /usr/src/myapp/target/x86_64-unknown-linux-musl/release/indiproj2 .
CMD ["./indiproj2"]
```
This Dockerfile uses a multi-stage build to keep the final image as small and secure as possible.

### 6. Bulid the Docker Image
Run `docker build -t tarot .` to create an image of the app.
```
[+] Building 108.0s (16/16) FINISHED                       docker:desktop-linux
 => [internal] load build definition from Dockerfile                       0.0s
 => => transferring dockerfile: 657B                                       0.0s
 => [internal] load metadata for docker.io/library/alpine:latest           1.5s
 => [internal] load metadata for docker.io/library/rust:1.75               0.8s
 => [internal] load .dockerignore                                          0.0s
 => => transferring context: 2B                                            0.0s
 => [builder 1/6] FROM docker.io/library/rust:1.75@sha256:87f3b2f93b82995  0.0s
 => [internal] load build context                                          0.5s
 => => transferring context: 392.41kB                                      0.3s
 => [stage-1 1/4] FROM docker.io/library/alpine:latest@sha256:c5b1261d6d3  0.9s
 => => resolve docker.io/library/alpine:latest@sha256:c5b1261d6d3e4307162  0.1s
 => => sha256:4abcf20661432fb2d719aaf90656f55c287f8ca915d 3.41MB / 3.41MB  0.2s
 => => sha256:c5b1261d6d3e43071626931fc004f70149baeba2c8e 1.64kB / 1.64kB  0.0s
 => => sha256:6457d53fb065d6f250e1504b9bc42d5b6c65941d57532c0 528B / 528B  0.0s
 => => sha256:05455a08881ea9cf0e752bc48e61bbd71a34c029bb1 1.47kB / 1.47kB  0.0s
 => => extracting sha256:4abcf20661432fb2d719aaf90656f55c287f8ca915dc1c92  0.3s
 => CACHED [builder 2/6] WORKDIR /usr/src/myapp                            0.0s
 => [builder 3/6] COPY ./ ./                                               7.3s
 => [stage-1 2/4] WORKDIR /root/                                           0.1s
 => [stage-1 3/4] RUN apk --no-cache add ca-certificates                   1.8s
 => [builder 4/6] RUN apt-get update && apt-get install -y musl-tools      5.6s 
 => [builder 5/6] RUN rustup target add x86_64-unknown-linux-musl          9.1s 
 => [builder 6/6] RUN cargo build --release --target x86_64-unknown-linu  83.0s
 => [stage-1 4/4] COPY --from=builder /usr/src/myapp/target/x86_64-unknow  0.2s
 => exporting to image                                                     0.2s
 => => exporting layers                                                    0.2s
 => => writing image sha256:ae034a843d1e4f5809662c7d6c06aa5e7fde35aea6906  0.0s
 => => naming to docker.io/library/tarot                                   0.0s

View build details: docker-desktop://dashboard/build/desktop-linux/desktop-linux/jclf0xds5quoz8w6obse3jx12

What's Next?
  View a summary of image vulnerabilities and recommendations → docker scout quickview
```
![screenshot docker image](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/image.png?ref_type=heads&inline=false)

### 7. Docker Container Test
Use command 'docker run -p 8080:8080 tarot' or click in the Docker APP to run the container, and visit through the browser to check out.
![screenshot container running](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/container.png?ref_type=heads&inline=false)
![screenshot a from docker](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/a-new.png?ref_type=heads&inline=false)

## Part III. CI/CD Pipeline

### 7. .gitlab-ci.yml Configuration
Create `.gitlab-ci.yml` file in the directory.
```
stages:
  - build
  - test
  - deploy

variables:
  DOCKER_IMAGE_NAME: my-rust-app

build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo "build stage..."
  only:
    - main

test:
  stage: test
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - echo "Logging into Docker..."
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - echo "Building Docker image..."
    - docker build -f Dockerfile.test -t $DOCKER_IMAGE_NAME:test .
  script:
    - echo "Running tests inside Docker container..."
    - docker run $DOCKER_IMAGE_NAME:test
  after_script:
    - echo "Test stage complete."

deploy:
  stage: deploy
  image: alpine:latest
  script:
    - echo "Deploying application..."
  only:
    - main
```

This config file has multi stages, it will run automatically once `git push` signal was received on the main branch.

- Dockerfile.test has a build-in RUST environment for test stage.

### 8. CI/CD Pipeline Test
Run `git push` from the terminal, we shall see the running phase in the Gitlab page:
![screenshot test pass](https://gitlab.com/estrellas939/indiproj2/-/raw/main/pic/pipeline.png?ref_type=heads&inline=false)
Now CI/CD pipeline passed all the stages.

### 9. Future Insights
This actix-web app needs more improvements on the front-end, could be use a `index.html` for more straight forward interaction between users.
As for the CI/CD config file, it still needs more scripts for the build and deploy stage.

