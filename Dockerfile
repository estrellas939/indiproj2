# Stage 1: Build
FROM rust:1.75 as builder
WORKDIR /usr/src/myapp

# Copy source code
COPY ./ ./

# Install necessary tools and add musl target
RUN apt-get update && apt-get install -y musl-tools

# RUN cargo test

RUN rustup target add x86_64-unknown-linux-musl

# Build statically linked binaries for Alpine
RUN cargo build --release --target x86_64-unknown-linux-musl

# Stage 2: Run
FROM alpine:latest
WORKDIR /root/

#Install runtime dependencies
RUN apk --no-cache add ca-certificates

# Copy the built app from the builder
COPY --from=builder /usr/src/myapp/target/x86_64-unknown-linux-musl/release/indiproj2 .
CMD ["./indiproj2"]
